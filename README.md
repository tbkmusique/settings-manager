[![pipeline status](https://gitlab.com/tbkmusique/settings-manager/badges/master/pipeline.svg)](https://gitlab.com/tbkmusique/settings-manager/-/commits/master)
[![coverage report](https://gitlab.com/tbkmusique/settings-manager/badges/master/coverage.svg)](https://gitlab.com/tbkmusique/settings-manager/-/commits/master)
# Tbkmusique Settings Manager
Manage your angular service settings globally easily with
this simple package.

All settings defined with this package are **global**. This means
that every service injecting the `SettingsManager` service can
change the configuration of services with the `@ServiceWithSettings` decorator.
This is the primary goal of this package: **manage global settings**.

## Usage
First, import the IonicStorageModule and the SettingsModule in your AppModule:
```ts
@NgModule({
    imports: [
        IonicStorageModule.forRoot(),
        SettingsManagerModule.forRoot({key: '__settings__'})
    ]
})
class AppModule {}
```
The `key` options defines the storage key of the settings
in the ionic storage interface.

Then, for a service which needs settings:
```ts
@Injectable(...)
@ServiceWithSettings({
    myService: {
        a: 1, // 1 is default value
    }
})
class MyService {
    constructor(private settingsManager: SettingsManager){
        this.settingsManager.get().subscribe(settings=>{
            console.log(settings);
        });
    }
}
```

**Important**: you should always namespace your service settings since all
services settings will be merged together.

## How it works
The package `ServiceManager` will first merge all the settings passed
to decorator `@ServiceWithSettings` to a single object which will be the 
*default config*. You can access the default config with the `ServiceManager.defaultConfig`
property.

When you call `get()`, the ServiceManager will merge the default config
with the value in the storage and return the value.

When you call `set()`, the ServiceManager will merge the default config,
the value in storage and the argument of `set()`. It will storage the merged object
and return it.

## API

### SettingsManager
#### Methods
##### `set(settings: ServiceSettings): Observable<ServiceSettings>`
Set a settings value. Do not forget to namespace your settings!

Example:
```ts
settingsManager.set({ myService: {a:1} }).subscribe(settings=>{
    console.log(settings); // settings.myService.a == 1
});
```

##### `get(): Observable<ServiceSettings>`
Get the settings.

Example:
```ts
settingsManager.get().subscribe(settings=>{
    console.log(settings); // settings.myService.a == 1
});
```

#### Properties
##### `defaultSettings: ServiceSettings`
Object containing all the default settings.

##### `$ready: Observable<ServiceSettings>`
An observable which resolves when the SettingsManager
is ready to use.

##### `storageKey: string`
Storage key of the settings in the Ionic storage. It is the same as the `key` options in the NgModule options.