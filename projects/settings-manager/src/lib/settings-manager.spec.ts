import { TestBed } from '@angular/core/testing';

import { SettingsManager, ServicesWithSettings } from './settings-manager';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { SettingsManagerModule } from './settings-manager.module';
import { from } from 'rxjs';


describe('SettingsManager', () => {
  let service: SettingsManager;
  let storageSpy: jasmine.SpyObj<Storage>;
  /**
   * The SettingsManager has two configuration
   * points: what the Ionic Storage returns
   * and the value of the injected string
   * 'servicesWithSettings'
   */
  function setup(
    storageValue: any,
    servicesWithSettings: ServicesWithSettings
  ) {
    storageSpy = jasmine.createSpyObj('Storage', [
      'get', 'set', 'ready'
    ]);
    const ret = new Promise((r) => r(storageValue));
    storageSpy.get.and.returnValue(ret);
    TestBed.configureTestingModule({
      imports: [
        IonicStorageModule.forRoot(),
        SettingsManagerModule.forRoot({ key: '__settings__' })
      ],
      providers: [
        { provide: 'servicesWithSettings', useValue: servicesWithSettings },
        { provide: Storage, useValue: storageSpy }
      ]
    });
  }

  it('initializes a \'storageKey\' property from the NgModule options', () => {
    setup({}, []);
    service = TestBed.inject(SettingsManager);
    expect(service.storageKey).toEqual('__settings__');
  });

  it('initializes \'$ready\' property which resolves when IonicStorage is ready', (done) => {
    setup({ b: 2 }, [
      {
        constructor: null,
        settings: { a: 1 }
      }]);
    storageSpy.ready.and.returnValue(new Promise(r => r()));
    service = TestBed.inject(SettingsManager);
    expect(service.$ready).toBeDefined();
    service.$ready.subscribe(() => {
      expect(storageSpy.ready).toHaveBeenCalled();
      done();
    });
  });

  it('initializes \'defaultSettings\' property', () => {
    const servicesWithSettings = [
      { constructor: null, settings: { a: 1 } },
      { constructor: null, settings: { b: 2 } },
      { constructor: null, settings: { c: 3 } },
    ];
    setup({}, servicesWithSettings);
    service = TestBed.inject(SettingsManager);
    expect(service.defaultSettings)
      .toEqual({ a: 1, b: 2, c: 3 });
  });

  it('get() prioritizes the storage value over default', (done) => {
    setup({ a: true, b: false, c: true }, [
      { constructor: null, settings: { a: 1 } },
      { constructor: null, settings: { b: 2 } },
      { constructor: null, settings: { c: 3 } },
    ]);
    service = TestBed.inject(SettingsManager);
    service.get().subscribe((settings) => {
      expect(settings).toEqual({ a: true, b: false, c: true });
      done();
    });
  });

  it('set() sets and returns expected value', (done) => {
    setup({ a: 'get() has not been mocked correctly!' }, [
      { constructor: null, settings: { a: 1 } },
      { constructor: null, settings: { b: 2 } },
      { constructor: null, settings: { c: 3 } },
    ]);

    // When storage.set() is called,
    // We want to make sure it is called
    // with the good arguments
    storageSpy.set.and.callFake((k, sets) => {
      expect(k).toEqual('__settings__');
      expect(sets).toEqual({ a: 2, b: 2, c: 5, d: 12 });
      return new Promise(r => r(sets));
    });
    service = TestBed.inject(SettingsManager);

    // service.get(), used by service.set(),
    // will return {a:2} as storage value
    const getSpy = spyOn(service, 'get');
    getSpy.and.returnValue(from(new Promise(r => r({ a: 2 }))));

    // Call the function
    service.set({ c: 5, d: 12 }).subscribe((settings) => {
      expect(settings).toEqual({ a: 2, b: 2, c: 5, d: 12 });
      expect(getSpy).toHaveBeenCalled();
      done();
    });
  });
});
