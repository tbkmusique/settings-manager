import { ServiceSettings } from './settings-manager';

/**
 * Merge many ServiceSettings instances to
 * a single one.
 *
 * The overrides priority for keys is `from
 * right to left`.
 *
 * ex: `mergeSettings({a:{b:1, c:2}}, {a:{b:2}})`
 * -> `{a:{b:2, c:2}}`
 * @param sets the ServiceSettings instances to be merged
 */
export function mergeSettings(...sets: ServiceSettings[]): ServiceSettings{

  // create a new object
  const target = {};

  // deep merge the object into the target object
  const merger = (obj: any) => {
      for (const prop in obj) {
          if (obj.hasOwnProperty(prop)) {
              if (Object.prototype.toString.call(obj[prop]) === '[object Object]') {
                  // if the property is a nested object
                  target[prop] = mergeSettings(target[prop], obj[prop]);
              } else {
                  // for regular property
                  target[prop] = obj[prop];
              }
          }
      }
  };

  // iterate through all objects and
  // deep merge them with target
  for (const set of sets) {
      merger(set);
  }

  return target;
}
