import { NgModule, ModuleWithProviders } from '@angular/core';
import { ServiceSettings, ServicesWithSettings } from './settings-manager';

export const SERVICES_WITH_SETTINGS: ServicesWithSettings = [];

/**
 * Mark a service as needing settings
 * provided by the SettingsManager instance
 *
 * Settings will then be available by injecting
 * `SettingsManager` in your service.
 *
 * Example:
 *
 * ```ts
 * \@ServiceWithSettings({
 *    myService: {
 *      a: 1,
 *      b: "rah"
 *    }
 * })
 * class MyService {
 *    constructor(private settingsManager: SettingsManager){
 *      const settings = this.settingsManager.get();
 *      this.settingsManager.set({myService: {a:2}}); // 'myService.b' will keep its value
 *    }
 * }
 * ```
 *
 * **NOTE**:  Since settings from all your services using
 * this decorator will be merged into one single
 * object, you must namespace your service settings.
 *
 * **NOTE**: Settings are global! If you have multiple
 * instances of your service, they will all have the same
 * settings.
 *
 * @param settings settings specific to the service
 */
export function ServiceWithSettings(settings: ServiceSettings) {
  return (constructor: new () => void) => {
    SERVICES_WITH_SETTINGS.push({
      constructor,
      settings
    });
  };
}

export interface SettingsManagerOptions {
  key: string;
}

@NgModule({
  declarations: [],
  imports: [
  ],
  exports: []
})
export class SettingsManagerModule {
  static forRoot(options: SettingsManagerOptions): ModuleWithProviders {
    return {
      ngModule: SettingsManagerModule,
      providers: [
        { provide: 'SettingsManagerOptions', useValue: options },
        { provide: 'servicesWithSettings', useValue: SERVICES_WITH_SETTINGS }
      ]
    };
  }
}
