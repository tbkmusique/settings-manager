import { mergeSettings } from './utils';

describe('utils', () => {
    describe('mergeSettings()', () => {
        it('returns same if only one map', () => {
            const m = { a: 1, b: true, c: { d: 1, e: 'rah' } };
            expect(mergeSettings(m)).toEqual(m);
        });

        it('overrides root key', () => {
            const m = { a: 1, b: true, c: { d: 1, e: 'rah' } };
            const result = mergeSettings(m, {a: 2});
            m.a = 2;
            expect(result).toEqual(m);
        });

        it('overrides deep key', () => {
            const m = { a: 1, b: true, c: { d: 1, e: 'rah' } };
            const result = mergeSettings(m, {c: {d: 2}});
            m.c.d = 2;
            expect(result).toEqual(m);
        });

        it('doesnt alter other deep keys when changing deep key', () => {
            const m = { a: 1, b: true, c: { d: 1, e: 'rah' } };
            const result = mergeSettings(m, {c: {d: 2}});
            m.c.d = 2;
            expect(result.c.e).toEqual('rah');
        });

        it('overrides an overrride', () => {
            const m = { a: 1, b: true, c: { d: 1, e: 'rah' } };
            expect(mergeSettings(m, {c: {d: 2}}, {c: {d: 1}}))
                .toEqual(m);
        });

        it('ignores null arguments', () => {
            const m = { a: 1, b: true, c: { d: 1, e: 'rah' } };
            const result = mergeSettings(m, null, {b: false});
            m.b = false;
            expect(result).toEqual(m);
        });
    });
  });
