import { Injectable, Inject } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable, from } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { mergeSettings } from './utils';
import { SettingsManagerModule, SettingsManagerOptions } from './settings-manager.module';

export type ServiceSettings = { [k: string]: any | ServiceSettings };
export type ServicesWithSettings = {constructor: new () => void, settings: ServiceSettings }[];


/**
 * Manages the settings.
 * You **MUST** import the IonicStorageModule
 * from package '@ionic/storage' to use this service.
 *
 * The following line (with 'dynamic'), fixes
 * the this issue: https://github.com/ng-packagr/ng-packagr/issues/641
 */
// @dynamic
@Injectable({
  providedIn: SettingsManagerModule
})
export class SettingsManager {

  get $ready(): Observable<void> {
    return from(this.storage.ready().then(_ => {}));
  }

  get storageKey(): string { return this.options.key; }

  /**
   * Object containing the default settings.
   */
  get defaultSettings(): ServiceSettings {
    return mergeSettings.apply(null, this.services.map(s => s.settings));
  }

  constructor(
    @Inject('SettingsManagerOptions') private options: SettingsManagerOptions,
    @Inject('servicesWithSettings') private services: ServicesWithSettings,
    private storage: Storage
    ) {}

  /**
   * Get the settings
   */
  get(): Observable<ServiceSettings>{
    return from(
      this.storage.get(this.storageKey).then(result => {
        return mergeSettings(this.defaultSettings, result);
      })
    );
  }

  /**
   * Change the settings value.
   *
   * @param settings the partial *or complete) settings to be set
   */
  set(settings: ServiceSettings): Observable<ServiceSettings>{
    return this.get().pipe(
      mergeMap((current) => {
        return from(
          this.storage.set(this.storageKey, mergeSettings(this.defaultSettings, current, settings))
        );
      })
    );
  }
}
