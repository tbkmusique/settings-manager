/*
 * Public API Surface of settings-manager
 */

export * from './lib/settings-manager';
export * from './lib/settings-manager.module';
export * from './lib/utils';
